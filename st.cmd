require adcore
require admisc
require asyn
require busy
require adps4000a, bdfc2ad8

#
#- avoid messages 'callbackRequest: cbLow ring buffer full'
callbackSetQueueSize(10000)

#- 10 MB max CA request
epicsEnvSet("DEVICE_NAME",                  "Scope-001")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("PORT",                         "PICO")
#epicsEnvSet("NUM_SAMPLES",                  "500")
epicsEnvSet("MAX_SAMPLES",                  "100000")
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")

#- create a PicoScope 4000A driver
#- PS4000AConfig(const char *portName, int numSamples, int dataType,
#-               int maxBuffers, int maxMemory, int priority, int stackSize)
#- dataType == NDInt32 == 4
PS4000AConfig("$(PORT)", $(MAX_SAMPLES), 4, 0, 0)

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

dbLoadRecords("ps4000a.template","P=$(PREFIX),R=,PORT=$(PORT),ADDR=0,TIMEOUT=1,MAX_SAMPLES=$(MAX_SAMPLES)")

#- individual input channels

iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=0, ADDRCH=A0, NAME=A")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=1, ADDRCH=A1, NAME=B")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=2, ADDRCH=A2, NAME=C")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=3, ADDRCH=A3, NAME=D")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=4, ADDRCH=A4, NAME=E")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=5, ADDRCH=A5, NAME=F")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=6, ADDRCH=A6, NAME=G")
iocshLoad("$(E3_CMD_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=7, ADDRCH=A7, NAME=H")

# EXAMPLE FOR MORE DE
# For more devices set unique PORT for teh device and ADDRCH for each channel

#epicsEnvSet("PORT",                         "PICO2")
#epicsEnvSet("DEVICE_NAME",                  "Scope-002")
#epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
#
#PS4000AConfig("$(PORT)", $(MAX_SAMPLES), 4, 0, 0)
#
#dbLoadRecords("ps4000a.template","P=$(PREFIX),R=,PORT=$(PORT),ADDR=0,TIMEOUT=1,MAX_SAMPLES=$(MAX_SAMPLES)")
#
#iocshLoad("$(E3_IOCSH_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=0, ADDRCH=B0, NAME=A")
#iocshLoad("$(E3_IOCSH_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=1, ADDRCH=B1, NAME=B")
#iocshLoad("$(E3_IOCSH_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=2, ADDRCH=B2, NAME=C")
#iocshLoad("$(E3_IOCSH_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=3, ADDRCH=B3, NAME=D")
#iocshLoad("$(E3_IOCSH_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=4, ADDRCH=B4, NAME=E")
#iocshLoad("$(E3_IOCSH_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=5, ADDRCH=B5, NAME=F")
#iocshLoad("$(E3_IOCSH_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=6, ADDRCH=B6, NAME=G")
#iocshLoad("$(E3_IOCSH_TOP)/iocsh/channel_ndevices.iocsh", "ADDR=7, ADDRCH=B7, NAME=H")

iocInit()

